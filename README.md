mysterycoin


RPC Port: 37215
P2P Port: 37214

Algorithm: X11 POW/POS

Short: MYST


Total coin: 2.8 Million POW, 16.18% POS First Year
Block time: 1.5 minute
Stake Interest: 16.18%



X11 Proof Of Work Stage: Block 1 to 5606

Reward:

1.618 x 987 		1 - 500	       798483
1.618 x 610 		501 - 1000     493490
1.618 x 987		1001 - 1100    159696
1.618 x 610		1101 - 1400    296094
1.618 x 377		1401 - 2000    365991
1.618 x 233		2001 - 3000    376994
1.618 x 144		3001 - 3400    93196
1.618 x 377		3500 - 3600    60998
1.618 x 89		3601 - 4600    93196.8
1.618 x 55		4600 - 5606    88990

Total: 2827128


After block 5606 any proof of work sent to the daemon will be rejected.

16.18% Proof of Stake First Year

